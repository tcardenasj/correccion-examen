**Correccion del Examen Interciclo**

1. Multiproceso

Ejecutar script:
python Cardenas_Tatiana_Correccionmultiprocess.py

Para la realizacion del ejercicio propuesto importamos la libreria  que se intalo previamente en python. Posteriormente
puse el mismo codigo de secuencial en paralelo donde finalmente en la seccion de if __name__ == '__main__': se realizara el proceso en 
paralelo en donde se definira un rango de procesos que se desean luego se llamara al multiprocessing.Process con el target de la funcion
los argumentos que posee. El arreglo vamos a adjuntar con el proceso que realizamos y finalmente realizamos un start() y un join(). 

![1](https://gitlab.com/tcardenasj/correccion-examen/-/blob/master/multiprocessing.png)



Ejecutar script
mpiexec -n 10 python Cardenas_Tatiana_CorreccionMPI.py.py

Para la realizacion del ejercicio propuesto importamos la librebria de MPI from mpi4py que se instalo previamente en python.
Posteriormente inicializamos las variables que vamos a utiliza, ademas de las propiedades del MPI. Se utiliza el numtasks-1 ya que este hace
referencia al proceso Master el cual enviara cada parte de la matriz a los distinos subprocesos que se declaren en la linea de comandos 
como por ejemplo "-n 10". Ahora se realizara el envio de datos a cada trabajador que esta en espera trabajando con la misma cantidad de filas
para todos los rangos. A continiacion se recive los datos datos de los subprocesos y se adjuntan al arreglo. Finalmente trabajamos con los subprocesos
si el taskid es mayor al Master (0) se va a recibir todo de la fuente del master y dentro de los datos recibidos se ira sumando un contador
para el minimo y el maximo. 