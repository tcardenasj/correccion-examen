#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 22:00:00 2020

@author: Tatiana Cardenas
"""

import time
from mpi4py import MPI
from random import randint
import numpy as np



comm = MPI.COMM_WORLD
#taskid = rank 
taskid = comm.Get_rank()
# Size = numtasks
numtasks = comm.Get_size()
np.random.RandomState(100)

#N = filas
N=4000000
maximum=8
minimum=4
MASTER=0

arr = np.random.randint(0, 10, size=[N, 10])

#ar = arr.tolist()
start = time.time()
MPI.Init


if taskid == 0:  
    
    #Es size -1 ya que este es el proceso Master que envia cada parte de la matriz a los
    #subprocesos trabajadores
    averow = int(N/(numtasks-1))
    extra = N%(numtasks-1)
    offset = 0
    
    # Envio de los datos a cada trabajador que esta en espera por lo que se divio por rangos procurando que cada
    #proceso trabaje con las mismas antidades de filas
    
    #Va desde el rango 1 hasta size 
    
    for destino in range(1,numtasks):
        rows = averow+1 if destino <= extra else averow 
        #Se envia el offset al destino
        comm.send(offset, destino)
        #Se envian las filas al destino
        comm.send(rows, destino)
        #Se envia el arreglo offset asignando la suma de las columna y el offset para que vaya al destino 
        comm.send(arr[offset:(rows+offset)],destino)
        offset+=rows
    
    resultsPar=[]
    
    #Recuperacion de datos 
    for i in range(1,numtasks):
        extracto=comm.recv(source=i)
        
        resultsPar.append(extracto)
        
    end=time.time()
    

    print('MPI Process took %.3f ms \n' % ((end-start)*1000))

#Recorre todos los datos y los almacena en un nuevo arreglo     
if taskid > MASTER:
    offse=comm.recv(source=MASTER)
    row=comm.recv(source=MASTER)
    data=comm.recv(source=MASTER)
    #print(data)
    res=[]
    for i in data:
        count = 0
        for j in i:
            if minimum <= j <= maximum:
                count += 1
        res.append(count)
    comm.send(res,0)


MPI.Finalize
